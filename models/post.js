var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var post = new Schema({
  title: String,
  author: String,
  content: String, 
  postDate: String,
  imageUrl: String
});

module.exports = mongoose.model('post', post);
