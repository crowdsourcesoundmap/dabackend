var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var apikey = new Schema({
  key: String
});

module.exports = mongoose.model('APIKEY', apikey);
