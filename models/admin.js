var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var admin = new Schema({
  uname: String
});

module.exports = mongoose.model('admin', admin);
